#!/bin/bash

dnf update -y
cd /var/www/laravel
yum -y update
dnf install -y httpd wget php-fpm php-mysqli php-json php php-devel
curl -s https://getcomposer.org/installer | php
php composer.phar install
mv composer.phar /usr/local/bin/composer
composer install
cp .env.example .env
php artisan key:generate


chgrp -R apache /home/ec2-user/
find /home/ec2-user/ -type d -exec chmod g+rx {} +
find /home/ec2-user/ -type f -exec chmod g+r {} +
chown -R ec2-user /home/ec2-user/
find /home/ec2-user/ -type d -exec chmod u+rwx {} +
find /home/ec2-user/ -type f -exec chmod u+rw {} +

sed -i "s|AllowOverride None|AllowOverride All|g"  /etc/httpd/conf/httpd.conf
sed -i "s|/var/www/html|/var/www/laravel/public|g"  /etc/httpd/conf/httpd.conf
sed -i "s|index.html|index.php|g" /etc/httpd/conf/httpd.conf


chown -R ec2-user:ec2-user /var/www/laravel
useradd -G ec2-user apache
chgrp -R apache storage bootstrap/cache
chmod -R ug+rwx storage bootstrap/cache

sudo systemctl start httpd
sudo systemctl start php-fpm
sudo systemctl enable php-fpm
